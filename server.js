const express = require('express');
const path = require('path');

const isDev = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 5000;

// Multi-process to utilize all CPU cores.

const app = express();

// Priority serve any static files.
app.use(express.static(path.join(__dirname, 'build')));

// Answer API requests.
app.get('/api', (req, res) => {
  res.set('Content-Type', 'application/json');
  res.send('{"message":"Piu-Piu!"}');
});

// All remaining requests return the React app, so it can handle routing.
app.get('/', (request, response) => {
  response.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.listen(PORT, () => {
  console.error(
    `Node ${
      isDev ? 'dev server' : `cluster worker ${process.pid}`
    }: listening on port ${PORT}`
  );
});
