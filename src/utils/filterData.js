const parseBirthDate = (date = '') => {
  let year = parseFloat(date);
  // No ABY dates are present in the API data!!!!!!
  if (date.includes('BBY')) {
    year = -year;
  }

  return year;
};

const filterBirthFunc = (date, birthConditions) => {
  const number = parseBirthDate(date);
  if (Number.isNaN(number)) {
    return false;
  }
  const { min, max } = birthConditions;
  if (min && max) {
    return -min <= number && number <= max;
  }
  if (min) {
    return number >= -min;
  }
  if (max) {
    return number <= max;
  }

  return false;
};

const filterPeopleData = (data, conditions, isORRelation) => {
  const conditionsArr = Object.keys(conditions).filter((key) => {
    if (!conditions[key]) return false;
    if (
      typeof conditions[key] === 'object' &&
      !Object.values(conditions[key]).some((item) => !!item)
    )
      return false;
    return true;
  });

  const filterCB = (item) => {
    const everyOrSomeCB = (key) => {
      if (key === 'birth_year') {
        if (!conditions[key].min && !conditions[key].max) return true;
        return filterBirthFunc(item[key], conditions[key]);
      }
      return item[key] && item[key].includes(conditions[key]);
    };

    return isORRelation
      ? conditionsArr.some(everyOrSomeCB)
      : conditionsArr.every(everyOrSomeCB);
  };

  const result = data.filter(filterCB);
  return result;
};

export default filterPeopleData;
