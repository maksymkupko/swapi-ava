/* eslint-disable camelcase */
import FilmItem from 'components/FilmItem/FilmItem';
import PlanetItem from 'components/PlanetItem/PlanetItem';
import SpeciesItem from 'components/SpeciesItem/SpeciesItem';
import VehicleItem from 'components/VehicleItem/VehicleItem';
import DragTypes from 'constants/dragTypes';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Accordion, ListGroup } from 'react-bootstrap';
import { useAccordionButton } from 'react-bootstrap/AccordionButton';
import { useDrag } from 'react-dnd';

function PeopleListItem(props) {
  const {
    name,
    height,
    mass,
    hair_color,
    skin_color,
    eye_color,
    birth_year,
    gender,
    homeworld,
    films,
    species,
    vehicles,
    starships,
    url
  } = props;
  const [{ isDragging }, drag] = useDrag(() => ({
    type: DragTypes.PeopleItem,
    item: { url },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging()
    })
  }));
  const eventKey = url;
  const [isOpened, setIsOpened] = useState(false);
  const onClick = () => setIsOpened(!isOpened);

  const decoratedOnClick = useAccordionButton(eventKey, onClick);

  const filmsList = films.length
    ? films.map((film) => <FilmItem key={film} url={film} />)
    : 'no data avaliable';

  const speciesList = species.length
    ? species.map((item) => <SpeciesItem key={item} url={item} />)
    : 'no data avaliable';

  const vehiclesList = vehicles.length
    ? vehicles.map((item) => (
        <VehicleItem key={item} url={item} readyToFetch={isOpened} />
      ))
    : 'no data avaliable';

  const starshipsList = starships.length
    ? starships.map((item) => (
        <VehicleItem key={item} url={item} readyToFetch={isOpened} />
      ))
    : 'no data avaliable';
  return (
    <div
      ref={drag}
      style={{
        opacity: isDragging ? 0.5 : 1,
        cursor: 'move'
      }}>
      <Accordion.Item eventKey={eventKey}>
        <Accordion.Header onClick={decoratedOnClick}>{name}</Accordion.Header>
        <Accordion.Body>
          <ListGroup>
            <ListGroup.Item>Height: {height}</ListGroup.Item>
            <ListGroup.Item>Mass: {mass}</ListGroup.Item>
            <ListGroup.Item>Hair color: {hair_color}</ListGroup.Item>
            <ListGroup.Item>Skin color: {skin_color}</ListGroup.Item>
            <ListGroup.Item>Eye color: {eye_color}</ListGroup.Item>
            <ListGroup.Item>Birth year: {birth_year}</ListGroup.Item>
            <ListGroup.Item>Gender: {gender}</ListGroup.Item>
            <ListGroup.Item>
              Homeworld: <PlanetItem readyToFetch={isOpened} url={homeworld} />
            </ListGroup.Item>
            <ListGroup.Item>Films: {filmsList}</ListGroup.Item>
            <ListGroup.Item>Species: {speciesList}</ListGroup.Item>
            <ListGroup.Item>Vehicles: {vehiclesList}</ListGroup.Item>
            <ListGroup.Item>Starships: {starshipsList}</ListGroup.Item>
          </ListGroup>
        </Accordion.Body>
      </Accordion.Item>
    </div>
  );
}

PeopleListItem.propTypes = {
  name: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  mass: PropTypes.string.isRequired,
  hair_color: PropTypes.string.isRequired,
  skin_color: PropTypes.string.isRequired,
  eye_color: PropTypes.string.isRequired,
  birth_year: PropTypes.string.isRequired,
  gender: PropTypes.string.isRequired,
  homeworld: PropTypes.string.isRequired,
  films: PropTypes.arrayOf(PropTypes.string).isRequired,
  species: PropTypes.arrayOf(PropTypes.string).isRequired,
  vehicles: PropTypes.arrayOf(PropTypes.string).isRequired,
  starships: PropTypes.arrayOf(PropTypes.string).isRequired,
  url: PropTypes.string.isRequired
};

export default PeopleListItem;
