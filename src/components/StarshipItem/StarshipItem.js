import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useLazyGetStarshipQuery } from 'store/api';

function StarshipItem(props) {
  const { url, readyToFetch } = props;
  const [trigger, result] = useLazyGetStarshipQuery();
  const { isLoading, isFetching, isError, isSuccess, data } = result;

  useEffect(() => {
    if (readyToFetch) {
      trigger(url, true);
    }
  }, [readyToFetch]);

  return (
    <>
      {(isLoading || isFetching) && <span>Loading...</span>}
      {isError && <span>Error while loading Starship</span>}
      {isSuccess && data && (
        <p>
          Name: {data.name}/ Model: {data.model}
        </p>
      )}
    </>
  );
}

StarshipItem.propTypes = {
  url: PropTypes.string.isRequired,
  readyToFetch: PropTypes.bool.isRequired
};

export default StarshipItem;
