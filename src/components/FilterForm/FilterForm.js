/* eslint-disable camelcase */
import CustomField from 'components/CustomField/CustomField';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import {
  Button,
  Col,
  FloatingLabel,
  FormCheck,
  FormControl,
  FormGroup,
  FormLabel,
  FormSelect,
  FormText,
  Row
} from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useGetFilmsQuery } from 'store/api';
import {
  changeFilter,
  filterInitialState,
  resetFilter,
  selectORRelation,
  switchORRelation
} from 'store/filter/filterSlice';
import { selectAllSpecies } from 'store/species/speciesSlice';
import * as Yup from 'yup';

function FilterForm() {
  const { filmsValues = [] } = useGetFilmsQuery(undefined, {
    selectFromResult: ({ data }) => ({
      filmsValues: data?.results.map(({ title, url }) => ({
        title,
        url
      }))
    })
  });

  const species = useSelector(selectAllSpecies);
  const isORRelation = useSelector(selectORRelation);
  const filterType = !isORRelation ? 'AND' : 'OR';
  const dispatch = useDispatch();
  const filmsOptions = filmsValues.map(({ title, url }) => (
    <option key={url} value={url}>
      {title}
    </option>
  ));
  const speciesOptions = species.map(({ name, url }) => (
    <option key={url} value={url}>
      {name}
    </option>
  ));
  const { birth_year, ...otherConds } = filterInitialState.conditions;
  const initialValues = {
    ...birth_year,
    ...otherConds
  };

  const validationSchema = Yup.object().shape({
    films: Yup.string(),
    species: Yup.string(),
    birthYearBBY: Yup.number()
      .typeError('you must specify a number')
      .positive('Should be a positive number')
      .integer('Should be an integer '),

    birthYearABY: Yup.number()
      .typeError('you must specify a number')
      .positive('Should be a positive number')
      .integer('Should be an integer ')
  });
  const onSubmit = (vals) => {
    const valsArr = Object.values(vals);

    if (valsArr.every((item) => !item)) {
      dispatch(resetFilter());
      return;
    }
    dispatch(changeFilter(vals));
  };
  const onReset = () => dispatch(resetFilter());

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialValues}
      onSubmit={onSubmit}
      onReset={onReset}>
      {(formikProps) => {
        const { resetForm, handleChange } = formikProps;

        return (
          <Form>
            <FormGroup className="mb-3">
              <FormLabel>Film</FormLabel>
              <CustomField name="films" component={FormSelect}>
                <option value="">Not selected</option>
                {filmsOptions}
              </CustomField>
            </FormGroup>
            <FormGroup className="mb-3">
              <FormLabel>Species</FormLabel>
              <CustomField name="species" component={FormSelect}>
                <option value="">Not selected</option>
                {speciesOptions}
              </CustomField>
            </FormGroup>
            <FormGroup className="mb-3">
              <Row>
                <Col>
                  <FloatingLabel label="BBY">
                    <CustomField name="min" component={FormControl} />
                    <div style={{ minHeight: '48px' }}>
                      <ErrorMessage name="min">
                        {(msg) => (
                          <FormText className="mt-0 text-danger">
                            {msg}
                          </FormText>
                        )}
                      </ErrorMessage>
                    </div>
                  </FloatingLabel>
                </Col>
                <Col>
                  <FloatingLabel label="ABY">
                    <CustomField name="max" component={FormControl} />
                    <div style={{ minHeight: '48px' }}>
                      <ErrorMessage name="max">
                        {(msg) => (
                          <FormText className="mt-0 text-danger">
                            {msg}
                          </FormText>
                        )}
                      </ErrorMessage>
                    </div>
                  </FloatingLabel>
                </Col>
              </Row>
            </FormGroup>
            {/* <Row className="justify-content-between align-items-start">
              <Button type="submit" variant="success" className="mb-3">
                Apply filter
              </Button> */}
            <Button
              className="w-100 mb-2"
              type="reset"
              onClick={() => resetForm}
              variant="danger">
              Reset filter
            </Button>
            {/* </Row> */}
            <FormCheck
              type="switch"
              id="custom-switch"
              onChange={() => dispatch(switchORRelation())}
              label={`Filter type is ${filterType}`}
            />
          </Form>
        );
      }}
    </Formik>
  );
}

FilterForm.propTypes = {};

export default FilterForm;
