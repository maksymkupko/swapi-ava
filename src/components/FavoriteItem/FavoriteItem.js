import React from 'react';
import PropTypes from 'prop-types';
import { useGetPeopleByIdQuery } from 'store/api';
import { deleteFavoritesItems } from 'store/favorites/favoritesSlice';
import { CloseButton } from 'react-bootstrap';
import { useDispatch } from 'react-redux';

function FavoriteItem(props) {
  const dispatch = useDispatch();
  const { id } = props;
  const { data, isLoading, isFetching, isSuccess } = useGetPeopleByIdQuery(id);
  const onCloseClick = () => dispatch(deleteFavoritesItems(id));
  return (
    <>
      {(isLoading || isFetching) && (
        <div className="border mb-2 mx-2">Loading...</div>
      )}
      {isSuccess && data && (
        <div className="border mb-2 mx-2 p-1 d-flex justify-content-between">
          {data.name}
          <CloseButton onClick={onCloseClick} />
        </div>
      )}
    </>
  );
}

FavoriteItem.propTypes = {
  id: PropTypes.string.isRequired
};

export default FavoriteItem;
