import FavoritesList from 'components/FavoritesList/FavoritesList';
import DragTypes from 'constants/dragTypes';
import React from 'react';
import { Container } from 'react-bootstrap';
import { useDrop } from 'react-dnd';
import { useDispatch } from 'react-redux';
import { addFavoriteItem } from 'store/favorites/favoritesSlice';

function FavoritesSection() {
  const dispatch = useDispatch();
  // eslint-disable-next-line no-unused-vars
  const [_, drop] = useDrop(() => ({
    accept: DragTypes.PeopleItem,
    drop: (item) => dispatch(addFavoriteItem(item))
  }));
  return (
    <Container style={{ height: '100%' }}>
      <div ref={drop} className="border h-100">
        <FavoritesList />
      </div>
    </Container>
  );
}

FavoritesSection.propTypes = {};

export default FavoritesSection;
