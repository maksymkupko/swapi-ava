import PropTypes from 'prop-types';
import React from 'react';
import { useGetSpeciesQuery } from 'store/api';

function SpeciesItem(props) {
  const { url } = props;

  const { isLoading, isFetching, isError, isSuccess, data } =
    useGetSpeciesQuery(url);

  return (
    <>
      {(isLoading || isFetching) && <span>Loading...</span>}
      {isError && <span>Error while loading Species</span>}
      {isSuccess && data && <p>{data.name}</p>}
    </>
  );
}

SpeciesItem.propTypes = {
  url: PropTypes.string.isRequired
};

export default SpeciesItem;
