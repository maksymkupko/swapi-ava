import { useField, useFormikContext } from 'formik';
import PropTypes from 'prop-types';
import React from 'react';

function CustomField(props) {
  const [field] = useField(props);
  const { submitForm } = useFormikContext();

  const { children } = props;
  const onChange = (e) => {
    field.onChange(e);
    submitForm();
  };
  const { component } = props;
  const Component = component;
  return (
    <Component {...field} onChange={onChange}>
      {children}
    </Component>
  );
}
CustomField.defaultProps = {
  children: null
};
CustomField.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  name: PropTypes.string.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  component: PropTypes.object.isRequired
};

export default CustomField;
