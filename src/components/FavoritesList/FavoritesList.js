import FavoriteItem from 'components/FavoriteItem/FavoriteItem';
import React from 'react';
import { useSelector } from 'react-redux';
import { selectFavoritesIds } from 'store/favorites/favoritesSlice';

function FavoritesList() {
  const favoritesIds = useSelector(selectFavoritesIds);

  const list =
    // @ts-ignore
    favoritesIds?.map((id) => <FavoriteItem key={id} id={id} />) || [];
  return (
    <div className="border pb-5">
      <h4 className="text-center">Favorites</h4>
      {list}
    </div>
  );
}

FavoritesList.propTypes = {};

export default FavoritesList;
