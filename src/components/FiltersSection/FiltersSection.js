import FilterForm from 'components/FilterForm/FilterForm';
import React from 'react';
import { Container } from 'react-bootstrap';

function FiltersSection() {
  return (
    <Container>
      <h4 className="text-center">Filter</h4>
      <FilterForm />
    </Container>
  );
}

FiltersSection.propTypes = {};

export default FiltersSection;
