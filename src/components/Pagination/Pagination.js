import React from 'react';
import { Pagination as BSPagination } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { nanoid } from '@reduxjs/toolkit';

function Pagination(props) {
  const { count, perPage, onClick, currentPage } = props;
  const paginationLength = Math.ceil(count / perPage);
  const arr = new Array(paginationLength).fill(null);

  const paginationList = arr.map((item, index) => {
    const isActive = index + 1 === currentPage;

    return (
      <BSPagination.Item
        key={nanoid()}
        onClick={() => onClick(index + 1)}
        active={isActive}>
        {index + 1}
      </BSPagination.Item>
    );
  });

  return (
    <BSPagination className="justify-content-center">
      <BSPagination.First
        disabled={currentPage === 1}
        onClick={() => onClick(1)}
      />
      <BSPagination.Prev
        disabled={currentPage === 1}
        onClick={() => onClick(currentPage - 1)}
      />
      {paginationList}
      <BSPagination.Next
        disabled={currentPage === paginationLength}
        onClick={() => onClick(currentPage + 1)}
      />
      <BSPagination.Last
        disabled={currentPage === paginationLength}
        onClick={() => onClick(paginationLength)}
      />
    </BSPagination>
  );
}

Pagination.propTypes = {
  count: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  perPage: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Pagination;
