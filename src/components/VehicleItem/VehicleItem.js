import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useLazyGetVehicleQuery } from 'store/api';

function VehicleItem(props) {
  const { url, readyToFetch } = props;
  const [trigger, result] = useLazyGetVehicleQuery();
  const { isLoading, isFetching, isError, isSuccess, data } = result;

  useEffect(() => {
    if (readyToFetch) {
      trigger(url, true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [readyToFetch]);

  return (
    <>
      {(isLoading || isFetching) && <span>Loading...</span>}
      {isError && <span>Error while loading Vehicle</span>}
      {isSuccess && data && (
        <p>
          Name: {data.name}/ Model: {data.model}
        </p>
      )}
    </>
  );
}

VehicleItem.propTypes = {
  url: PropTypes.string.isRequired,
  readyToFetch: PropTypes.bool.isRequired
};

export default VehicleItem;
