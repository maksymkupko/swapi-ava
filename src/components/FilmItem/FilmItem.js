import React from 'react';
import PropTypes from 'prop-types';
import { useGetFilmsQuery } from 'store/api';

function FilmItem(props) {
  const { url } = props;
  const { film, loading } = useGetFilmsQuery(undefined, {
    selectFromResult: ({ data, isLoading }) => ({
      film: data?.results?.find((item) => item.url === url),
      loading: isLoading
    })
  });

  return (
    <>
      {loading && <p>Loading</p>}
      {film && <p>{film.title}</p>}
    </>
  );
}
FilmItem.propTypes = {
  url: PropTypes.string.isRequired
};

export default FilmItem;
