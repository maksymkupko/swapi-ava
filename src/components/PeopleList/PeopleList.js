import Pagination from 'components/Pagination/Pagination';
import PeopleListItem from 'components/PeopleListItem/PeopleListItem';
import React, { useState } from 'react';
import { Accordion, Container, Row, Spinner } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useGetPeopleQuery } from 'store/api';
import { selectFilter } from 'store/filter/filterSlice';
import filterPeopleData from 'utils/filterData';
// @ts-ignore
import styles from './PeopleList.module.scss';

export default function PeopleList() {
  const [currentPage, setCurrentPage] = useState(1);
  const filter = useSelector(selectFilter);
  const { data, isLoading, isError, isFetching } =
    useGetPeopleQuery(currentPage);
  const dataToRender = filter.enabled
    ? filterPeopleData(data?.results, filter.conditions, filter.isORRelation)
    : data?.results;

  const list = dataToRender?.length ? (
    dataToRender?.map((item) => <PeopleListItem key={item.url} {...item} />)
  ) : (
    <h3 className="text-center pt-4">
      No data to display. You can try to change filters...
    </h3>
  );
  const pagination =
    data && !isLoading ? (
      <Pagination
        onClick={setCurrentPage}
        currentPage={currentPage}
        count={data?.count}
        perPage={10}
      />
    ) : null;

  return (
    <Container>
      <div className={styles.wrapper}>
        {isError && <p className="text-danger">error occured</p>}
        {(isLoading || isFetching) && (
          <div className={styles.spinnerWrapper}>
            <Spinner
              className={styles.spinner}
              animation="border"
              variant="success"
            />
          </div>
        )}
        {!isLoading && data && (
          <>
            <Accordion className={styles.list}>{list}</Accordion>
            <Row className="justify-content-center">{pagination}</Row>
          </>
        )}
      </div>
    </Container>
  );
}
