import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useLazyGetPlanetQuery } from 'store/api';

function PlanetItem(props) {
  const { url, readyToFetch } = props;
  const [trigger, result] = useLazyGetPlanetQuery();
  const { isLoading, isFetching, isError, isSuccess, data } = result;

  useEffect(() => {
    if (readyToFetch) {
      trigger(url, true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [readyToFetch]);

  return (
    <>
      {(isLoading || isFetching) && <span>Loading...</span>}
      {isError && <span>Error while loading planet</span>}
      {isSuccess && data && <span>{data.name}</span>}
    </>
  );
}

PlanetItem.propTypes = {
  url: PropTypes.string.isRequired,
  readyToFetch: PropTypes.bool.isRequired
};

export default PlanetItem;
