import { configureStore } from '@reduxjs/toolkit';
import {
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist';
import swapi from './api';
import favoritesSlice, {
  persistedFavoritesReducer
} from './favorites/favoritesSlice';
import filterSlice from './filter/filterSlice';
import speciesSlice from './species/speciesSlice';

const store = configureStore({
  reducer: {
    [swapi.reducerPath]: swapi.reducer,
    [speciesSlice.name]: speciesSlice.reducer,
    [filterSlice.name]: filterSlice.reducer,
    [favoritesSlice.name]: persistedFavoritesReducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
      }
    }).concat(swapi.middleware)
});
const persistor = persistStore(store);
export { persistor };
export default store;
