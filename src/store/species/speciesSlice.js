const { createSlice, createEntityAdapter } = require('@reduxjs/toolkit');

const adapter = createEntityAdapter({
  selectId: (item) => item.url
});

const speciesSlice = createSlice({
  name: 'species',
  initialState: adapter.getInitialState(),
  reducers: {
    addSpecies: (state, action) => {
      adapter.setOne(state, action.payload);
    }
  }
});

export const { addSpecies } = speciesSlice.actions;
export const { selectAll: selectAllSpecies } = adapter.getSelectors(
  (state) => state.species
);
export default speciesSlice;
