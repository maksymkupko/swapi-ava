import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { addSpecies } from 'store/species/speciesSlice';

const baseUrl = process.env.REACT_APP_SWAPI;

const swapi = createApi({
  reducerPath: 'swapi',
  baseQuery: fetchBaseQuery({ baseUrl }),
  tagTypes: ['People', 'Starships', 'Planets', 'Films', 'Species', 'Vehicles'],
  endpoints: (builder) => ({
    getPeople: builder.query({
      query: (page = '1') => ({ url: `people?page=${page}` }),
      providesTags: (data) =>
        data
          ? [
              ...data.results.map(({ url }) => ({ type: 'People', id: url })),
              { type: 'People', id: 'LIST' }
            ]
          : [{ type: 'People', id: 'LIST' }]
    }),
    getPeopleById: builder.query({
      query: (id) => ({ url: id }),
      providesTags: (data) => (data ? [{ type: 'People', id: data.url }] : null)
    }),
    getStarship: builder.query({
      query: (url) => ({ url }),
      providesTags: ({ url }) => [{ type: 'Starships', id: url }]
    }),
    getPlanet: builder.query({
      query: (url) => ({ url }),
      providesTags: ({ url }) => [{ type: 'Planets', id: url }]
    }),
    getFilms: builder.query({
      query: () => ({ url: 'films' }),
      providesTags: ({ url }) => [
        { type: 'Films', id: url },
        { type: 'Films', id: 'List' }
      ]
    }),
    getSpecies: builder.query({
      query: (url) => ({ url }),
      providesTags: ({ url }) => [{ type: 'Species', id: url }],
      async onCacheEntryAdded(arg, { dispatch, cacheDataLoaded }) {
        const data = await cacheDataLoaded;
        dispatch(addSpecies(data.data));
      }
    }),
    getVehicle: builder.query({
      query: (url) => ({ url }),
      providesTags: ({ url }) => [{ type: 'Vehicles', id: url }]
    })
  })
});

export const {
  useGetPeopleQuery,
  useGetPeopleByIdQuery,
  useGetFilmsQuery,
  useLazyGetPlanetQuery,
  useGetSpeciesQuery,
  useLazyGetVehicleQuery,
  useLazyGetStarshipQuery
} = swapi;

export default swapi;
