import persistReducer from 'redux-persist/es/persistReducer';
import storage from 'redux-persist/lib/storage';

const { createSlice, createEntityAdapter } = require('@reduxjs/toolkit');

const adapter = createEntityAdapter({ selectId: (item) => item.url });
const favoritesSlice = createSlice({
  name: 'favorites',
  initialState: adapter.getInitialState(),
  reducers: {
    addFavoriteItem: (state, { payload }) => {
      adapter.addOne(state, payload);
    },
    deleteFavoritesItems: (state, { payload }) => {
      adapter.removeOne(state, payload);
    }
  }
});

const persistConfig = {
  key: favoritesSlice.name,
  storage
};

export const { addFavoriteItem, deleteFavoritesItems } = favoritesSlice.actions;
export const persistedFavoritesReducer = persistReducer(
  persistConfig,
  favoritesSlice.reducer
);
export const { selectIds: selectFavoritesIds } = adapter.getSelectors(
  (state) => state.favorites
);
export default favoritesSlice;
