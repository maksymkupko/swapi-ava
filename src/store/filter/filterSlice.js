/* eslint-disable no-param-reassign */
const { createSlice } = require('@reduxjs/toolkit');

export const filterInitialState = {
  enabled: false,
  isORRelation: false,
  conditions: {
    films: '',
    species: '',
    birth_year: {
      min: '',
      max: ''
    }
  }
};

const filterSlice = createSlice({
  name: 'filter',
  initialState: {
    ...filterInitialState
  },
  reducers: {
    changeFilter: (state, action) => {
      const { min, max, ...otherConds } = action.payload;
      state.enabled = true;
      state.conditions = { ...otherConds, birth_year: { min, max } };
    },
    resetFilter: () => ({
      ...filterInitialState
    }),
    switchORRelation: (state) => {
      state.isORRelation = !state.isORRelation;
    }
  }
});

export const { changeFilter, resetFilter, switchORRelation } =
  filterSlice.actions;
export const selectFilter = (state) => state.filter;
export const selectORRelation = (state) => state.filter.isORRelation;
export default filterSlice;
