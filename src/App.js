import FavoritesSection from 'components/FavoritesSection/FavoritesSection';
import FiltersSection from 'components/FiltersSection/FiltersSection';
import PeopleList from 'components/PeopleList/PeopleList';
import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Container>
        <Row>
          <Col md={3}>
            <FiltersSection />
          </Col>
          <Col md={6}>
            <PeopleList />
          </Col>
          <Col md={3}>
            <FavoritesSection />
          </Col>
        </Row>
      </Container>
    </DndProvider>
  );
}

export default App;
